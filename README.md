# Bioactive Conformational Ensemble (BCE)

Bioactive Conformational Ensemble (BCE) workflow.

## Quickstart

The workflow takes the protein and ligand PDB codes, along with other optional arguments, and is executed in the following order of steps:

```markdown
Ligand Extraction -> Ligand Parametrization -> REMD Equilibration -> REMD Production -> Clustering -> Computing Dihedrals -> Gaussian Energy Optimization
```

It can be executed starting from or up to any of these steps. Another option is to execute only one step or the whole workflow. This is achieved using the following optional arguments: 

```shell
-mode {only,upto,from,all}

-step {ligext,ligparam,REMDeq,REMDprod,clustering,dihedrals,gauss}
```

If these options are not specified, all workflow steps will be executed in the previously mentioned order.

To print a help message and see all arguments and options available, along with a short description of each, execute the workflow as a Python script with the `-h` option:

```shell
python BCE.py -h
```

## Examples

* Run workflow on molecule with PDB code `3P6H` containing a ligand with code `IBP` up to the clustering step (`-executionMode upto -executionStep clustering`). Use 8 processes per MD replica in REMD (`-procs 8`). If the directory `<test_directory>` does not exist, it is created and all workflow output files are written there.

```shell
python BCE.py -workingdir test_directory -procs 8 -executionMode upto -executionStep clustering 3p6h IBP
```

* Run previous workflow starting from the ligand parametrization step (`-executionMode from -executionStep ligparam`), but specify that each REMD simulation has a length of 0.01 nanoseconds (`-len 0.01`). Also set output to `--verbose` (this will print all debug messages).

```shell
python BCE.py -workingdir test_directory -procs 8 -len 0.01 -executionMode from -executionStep ligparam --verbose 3p6h IBP
```

* Run only REMD equilibration step (`-executionMode only -executionStep REMDeq`).

```shell
python BCE.py -workingdir test_directory -executionMode only -executionStep REMDeq --verbose 3p6h IBP
```

* Run the whole workflow, set the `--force` flag to remove the contents of `<test_directory>` and recompute everything.

```shell
python BCE.py -workingdir test_directory --force 3p6h IBP
```

--- 

* Python environment 

Create environment with conda pack:

[Download](https://drive.google.com/file/d/1v8YT7TUuNuwkC3-quzAAJPeEV6CtHM5Z/view?usp=sharing) and copy pack to target machine.

Unpack environment into directory "bce_env"
```
$ mkdir -p bce_env
$ tar -xzf bce.tar.gz -C bce_env
```

Activate environment with

```
$ source bce_env/bin/activate
```

This will load Python executable and libraries.

* Install BCE Workflow

From the main BCE Workflow directory, where `setup.py` file is located, install workflow module by doing:

```
$ python setup.py install --record installation.txt
```

* Load environment modules (tested on MareNostrum's login1)

module load gcc/5.4.0 mkl/2018.4 impi/2018.4 plumed/2.6.2 gcc/9.2.0 mkl/2018.4 intel/2018.4 gromacs/2020.4-plumed.2.6.2
module load amber/14
module load ambertools/17
module load openbabel/2.3.1
..
