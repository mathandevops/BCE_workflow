import pathlib
import logging
import pickle
from functools import wraps
from hashlib import blake2b

from numpy import ndarray

logger = logging.getLogger("BCE_workflow")


def bytify(e):
    if isinstance(e, ndarray):
        str_e = e.tobytes()
    else:
        str_e = bytes(str(e), encoding="utf-8")
    return str_e


def create_key(hash_key, *args):
    hash_key = bytify(hash_key)
    customhash = blake2b(key=hash_key, digest_size=16)
    for arg in args:
        str_arg = bytify(arg)
        customhash.update(str_arg)
    key = customhash.digest()
    return key


def persist_to_file(original_func):
    # TO-DO: this implementation of cache wont recalculate function
    # for a given workingdir if the parameters are different,
    # this needs to change
    @wraps(original_func)
    def decorator(*args, **kwargs):
        # pathlib.Path.cwd() is workflow's workingdir
        cwd = pathlib.Path.cwd()
        cache_dir = cwd / "cache"
        if not cache_dir.exists():
            cache_dir.mkdir()
        logger.info(f"using cache directory: {cache_dir}")
        _file_name = cache_dir / "postprocessing_cache.dat"
        func_name = original_func.__name__
        other_args = (func_name, *args)
        func_key = create_key(cwd, *other_args)
        logger.info(f"checking cache data for function {func_name}")

        try:
            with _file_name.open("rb") as f:
                cache = pickle.load(f)
        except FileNotFoundError:
            logger.info("cache file not found! creating new...")
            cache = {}

        if func_key not in cache.keys():
            result = original_func(*args, **kwargs)
            logger.info(f"saving {func_name} function output to cache")
            cache[func_key] = result
            # save updated cache
            with _file_name.open("wb") as f:
                pickle.dump(cache, f)
        else:
            result = cache[func_key]
            logger.info("returning result from cache")

        return result

    return decorator
