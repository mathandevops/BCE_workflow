import numpy as np
import matplotlib.pyplot as plt


def local_density(distances, cutoff):
    """Returns local density for a point,
    given an array of distances to that point
    and a distance cut-off value."""
    def func(x): return 1 if x < 0 else 0
    density = sum(map(func, distances - cutoff))
    return density


def local_delta(distances, densities, local_density):
    """Returns local delta value for a point,
    given an array of distances to it,
    its local density and
    an array of densities for other points. """
    zipped_data = zip(densities, distances)
    filtered_distances = list(filter(
        lambda tup: tup[0] > local_density,
        zipped_data))
    filtered_distances = list(map(lambda tup: tup[1], filtered_distances))
    try:
        delta = min(filtered_distances)
    except ValueError:
        delta = 0
    return delta


def get_densities(distances_mat, cutoff):
    """Returns an array of local densities for all points
    given a matrix of distances and a distance cutoff value. """
    frames = distances_mat.shape[0]
    densities = np.ndarray(frames)
    for idx in range(frames):
        distances = distances_mat[idx, :]
        distances = distances.reshape(len(distances))
        densities[idx] = local_density(distances, cutoff)
    return densities


def get_deltas(densities, distances_mat):
    """Returns an array of delta values for all points
    given an array of densities and the distance matrix. """
    frames = len(densities)
    deltas = np.empty(frames, dtype=list)
    max_density_idx = np.where(densities == densities.max())[0][0]
    for idx in range(frames):
        distances = distances_mat[idx, :]
        distances = distances.reshape(len(distances))
        if idx == max_density_idx:
            deltas[idx] = max(distances)
        else:
            local_density = densities[idx]
            delta_val = local_delta(distances, densities, local_density)
            deltas[idx] = delta_val
    return deltas


def compute_gamma(densities, deltas):
    gamma = list(map(lambda tup: tup[0] * tup[1], zip(densities, deltas)))
    return np.array(gamma)


def compute_quantities(mat, cutoff=1):
    densities = get_densities(mat, cutoff=cutoff)
    deltas = get_deltas(densities, mat)
    return densities, deltas

# auxiliary functions for visualization


def plot_X(X):
    plt.plot(X[:, 0], X[:, 1], 'o')
    plt.show()


def decision_graph(densities, deltas):
    plt.plot(densities, deltas, 'o')
    plt.show()


def plot_gamma(densities, deltas):
    gamma = compute_gamma(densities, deltas)
    plt.plot(range(len(gamma)), gamma, 'o')
    plt.show()


def get_cluster_centers(gamma, gamma_threshold, X):
    cluster_centers = np.where(gamma > gamma_threshold)
    clcenters_coord = []
    for idx in cluster_centers:
        clcenters_coord.append((X[idx, 0], X[idx, 1]))
    return clcenters_coord
