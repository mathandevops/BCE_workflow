import pathlib
import logging
from os import chdir
from shutil import rmtree

from BCEwf.utils.utils import run_subprocess

logger = logging.getLogger("BCE_workflow")


def assert_directory_exists(directory):
    try:
        assert directory.exists()
    except AssertionError as e:
        raise FileNotFoundError(
            f"{directory} not found! ") from e


def get_paths(
        workingdir,
        ligand_code,
        use_given_workingdir=False,
        force=False,
        acpype_path=None):
    workingdir = pathlib.Path(workingdir)

    if not use_given_workingdir:
        if force:
            # if workingdir exists, it is removed
            remove_workingdir(workingdir)
        workingdir = get_working_directory(workingdir)
    workingdir = workingdir.resolve()

    files_absolute_path = pathlib.Path(__file__).parent.absolute()
    aux_files_path = files_absolute_path / "resources"
    aux_files_path = pathlib.Path().home() / ".bce"
    assert_directory_exists(aux_files_path)

    if not acpype_path:
        try:
            # look for installed acpype
            stdout, stderr = run_subprocess("which acpype")
            assert stdout != ""
            acpype_path = stdout.strip()
        except Exception or AssertionError:
            # if not found, use file in aux_files_path
            acpype_path = aux_files_path / "acpype.py"
            assert_directory_exists(acpype_path)
    else:
        # use specified acpype
        acpype_path = acpype_path
        assert_directory_exists(acpype_path)

    # workflow steps directories
    acpype_dir = workingdir / f"{ligand_code}.acpype"
    gmx_dir = workingdir / "GMX"
    clusters_parent = workingdir / "CLUSTERS"
    dih_dir = workingdir / "dihedrals"
    gauss_dir = workingdir / "gaussian"
    postpro_dir = workingdir / "clustering_postprocessing"

    return (
        workingdir,
        aux_files_path,
        acpype_path,
        acpype_dir,
        gmx_dir,
        clusters_parent,
        dih_dir,
        gauss_dir,
        postpro_dir)


def get_working_directory(workingdir):
    """Get path of working directory for the workflow."""
    if workingdir.exists():
        parent_dir = workingdir.parent
        idx = 1
        while True:
            alt_dir = parent_dir / (str(workingdir.stem) + f"_{idx}")
            if not alt_dir.exists():
                workingdir = alt_dir
                break
            idx += 1
    logger.info(f"creating working directory {workingdir}")
    workingdir.mkdir()
    return workingdir


def remove_workingdir(workingdir):
    # if force recompute all steps, remove workingdir
    logger.info("forcing recalculation of all steps")
    if workingdir.exists():
        logger.info(f"cleaning {workingdir}...")
        rmtree(workingdir)


# @contextmanager
# def change_to_working_directory(directory):
#     """context manager for temporarily changing to directory and then back"""
#     cwd = pathlib.Path.cwd()
#     try:
#         logger.debug(f"changing from {cwd} to {directory}")
#         yield chdir(pathlib.Path(directory))
#     finally:
#         logger.debug(f"changing back to {cwd}")
#         chdir(cwd)

class change_to_working_directory:
    def __init__(self, directory):
        self.directory = directory
        self.cwd = pathlib.Path.cwd()

    def __enter__(self):
        logger.debug(f"changing from {self.cwd} to {self.directory}")
        chdir(pathlib.Path(self.directory))

    def __exit__(self):
        logger.debug(f"changing back to {self.cwd}")
        chdir(self.cwd)


def remove_backups(working_dir):
    """Remove back up files created by Gromacs"""
    logger.info("removing files backed up by Gromacs...")
    for p in working_dir.rglob("./#*"):
        p.unlink()
