import logging

from BCEwf.utils.config import BCE_header


def create_logger(verbose=False):
    if verbose:
        logging_level = logging.DEBUG
    else:
        logging_level = logging.INFO

    logging_format = "[%(filename)s:%(lineno)s] %(levelname)s: %(message)s"
    logging.basicConfig(level=logging_level, format=logging_format)
    logger = logging.getLogger("BCE_workflow")

    # log header
    logger.info(BCE_header)

    return logger
