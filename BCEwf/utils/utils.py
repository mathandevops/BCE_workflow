import pathlib
import logging
import shlex
import subprocess
import re

from numpy import zeros as np_zeros, pi

logger = logging.getLogger("BCE_workflow")


def run_subprocess(
        cmd,
        shell=False,
        raise_exc=True,
        stdin=None,
        msg=None,
        logfile=None,
        encoding="utf-8",
        errors="replace"):
    """Run a subprocess and capture stdout and stderr.

    Arguments:
    cmd (str): command to be executed
    raise_exc (boolean): raise exception is return code is 1
    stdin: where to send standard input
    msg: message to sent to stdin
    logfile (pathlib.Path): path to logfile to write stderr+stdout
    encoding: encoding used to decode stderr and stdout messages
    errors: decode errors input"""
    logger.debug("cmd: " + cmd)
    cmd = shlex.split(cmd)
    proc = subprocess.Popen(
        cmd,
        shell=shell,
        stdin=stdin,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE)

    # write input msg if corresponding
    stdout, stderr = proc.communicate(input=msg)
    returncode = proc.returncode
    stdout = stdout.decode(encoding=encoding, errors=errors)
    stderr = stderr.decode(encoding=encoding, errors=errors)

    logger.debug(f"cmd return code: {returncode}")
    # logger.debug(
    #   f"stderr: {stderr.decode(encoding=encoding, errors=errors)}")
    if returncode != 0 and raise_exc:
        raise Exception(stderr)

    if logfile:
        logfile = pathlib.Path(logfile)
        with logfile.open(mode="w") as f:
            f.write(stderr)
            f.write(stdout)

    return stdout, stderr


def get_charge(pdbfile):
    """get ligand charge from pdb file"""
    charge = 0
    with pdbfile.open(mode="r") as f:
        while True:
            line = f.readline()
            if line.startswith(("ATOM", "HETATM")):
                elements = line.split()
                if "+" in elements[-1] and "C1+" not in elements[-1]:
                    charge += 1
                if "-" in elements[-1]:
                    charge -= 1
            elif not line:
                break
    return charge


def check_cluster_percentage(
        cluster_log,
        cl_nmodels,
        cl_percentage,
        length):
    """calculate cluster percentage from clusters logfile."""
    total_snapshots = length * 1000
    num_total_cl = 0
    num_cl = 0
    sum_result = 0

    logger.info("checking cluster percentage...")
    with cluster_log.open(mode="r") as f:
        while True:
            line = f.readline()

            match = re.search(r"Found (\d+) clusters", line)
            if match:
                num_total_cl = match.groups()[0]
                logger.info(
                    f"total number of clusters: {num_total_cl}")
                continue

            #
            match = re.search(r"^\s+\d+", line)
            if match:
                string = match.string
                num_c = string.split("|")[1]
                num_c = int(num_c.split()[0])
                sum_result += num_c
                percentage = (sum_result / total_snapshots) * 100
                num_cl += 1

                logger.info(
                    f"percentage: {percentage:.2f}, NumCl: {num_cl}, "
                    f"Sum: {sum_result}, NumC: {num_c}")

                if percentage > cl_percentage:
                    if num_cl > cl_nmodels:
                        return (num_total_cl, 0)
                    else:
                        return (num_total_cl, num_cl)
            if not line:
                break

        return (num_total_cl, 0)


def get_rot_table(table_filepath="list-pdb-lig-sym-bynumberofROT.txt"):
    """create a dictionary out of
    <list-pdb-lig-sym-bynumberofROT.txt> file."""
    p = pathlib.Path(table_filepath)
    try:
        assert p.exists()
    except AssertionError:
        raise AssertionError(
            "<list-pdb-lig-sym-bynumberofROT.txt> not found! "
            "check path and try again.")

    datalist = p.read_text(encoding="utf-8").split("\n")
    rot_table = {}
    columns = datalist[1].replace("\"", "").replace("\t", " ").split(" ")
    columns[0] = "num_rot_bonds"

    first_keys = [*list(f"{i}ROT" for i in range(1, 13)),
                  "TEST LIGANDS", "MACROCYCLES", "DUAL"]
    keys_idxs = [datalist.index(f"#{k}") for k in first_keys]

    for k in range(len(keys_idxs)):
        try:
            section = datalist[keys_idxs[k] + 1:keys_idxs[k + 1]]
        except IndexError:
            section = datalist[keys_idxs[k]:]

        section = [elem.split("\t") for elem in section]
        section = [list(filter(lambda x:x != "", elem)) for elem in section]
        for elem in section:
            elem.insert(1, first_keys[k])

        for content in section:
            rot_table[content[0].lower()] = {
                col: val for col, val in zip(columns, content[1:])
            }
    return rot_table


def build_dih_matrix(dih_dir, dih_indexes=None, angle_unit="radians"):
    logger.info("building dihedral matrix")

    # find line where angles table starts
    dih1_file = dih_dir / "dih1.xvg"
    dih1 = dih1_file.read_text()
    dih1 = dih1.split("\n")
    dih1.pop(-1)
    for line in dih1:
        if line.startswith("@TYPE xy"):
            k = dih1.index(line) + 1
            break
    # total number of lines in dih1.xvg
    s = len(dih1)
    # number of dihedral xvg files in dihedrals directory
    if not dih_indexes:
        logger.info("using all dihedral xvg files")
        m = len(list(dih_dir.glob("dih*.xvg")))
        dih_index_range = range(1, m+1)
    else:
        logger.info("using provided list of dihedrals")
        m = len(dih_indexes)
        dih_index_range = dih_indexes
    # build zeros matrix
    dihedral_angles = np_zeros((s - k, m))

    for j in dih_index_range:
        dihname = f"dih{j}.xvg"
        dihfile = dih_dir / pathlib.Path(dihname)
        dih_contents = dihfile.read_text().split("\n")
        # remove last line since its empty
        dih_contents.pop(-1)
        dih_contents = dih_contents[k:]
        # get 2nd columns from angles table
        col2 = []
        for elem in dih_contents:
            try:
                elem = elem.split()[1]
            except IndexError:
                continue
            col2.append(float(elem))
        dihedral_angles[:, j - 1] = col2

    if angle_unit == "radians":
        logger.debug("converting dihedral angle units to radians")
        dihedral_angles = dihedral_angles * pi / 180

    return dihedral_angles
