"""Perform ligand extraction from a PDB file, given a ligand code."""

import pathlib
import logging
import requests

from BCEwf.utils.utils import run_subprocess
from BCEwf.utils.config import mmbpdb_url

logger = logging.getLogger("BCE_workflow")


def download_pdb_file(pdb_code, pdb_filename):
    """Download protein PDB file from MMB repository."""
    logger.info("attempting to download PDB file...")
    try:
        pdb_data = requests.get(mmbpdb_url.format(pdb_code=pdb_code))
        with pdb_filename.open("w") as f:
            f.write(pdb_data.text)
    except requests.exceptions.HTTPError as e:
        raise SystemExit(e)
    except requests.exceptions.RequestException as e:
        raise Exception("error fetching PDB file!") from e


def extract_ligand(input_file, ligand_code):
    """
    Extract all instances of ligand from PDB input file.
    Return first ligand instance found.
    """
    logger.info("Performing ligand extraction...")
    with input_file.open(mode="r") as fpdb:
        # extract all instances of ligand from pdb file
        ligand_dict = {}
        while True:
            line = fpdb.readline()
            if line.startswith("HETATM") and ligand_code.upper() in line:
                res = line[21:26].replace(" ", "")
                updated_line = ligand_dict.get(res, "")
                updated_line += line
                ligand_dict[res] = updated_line
            elif not line:
                break

    # choose first ligand (alphabetically)
    # get current working directory
    res = list(ligand_dict.keys())[0]
    lig_pdb = ligand_dict[res]
    return lig_pdb


def write_ligand_file(ligand_pdb, ligand_filename):
    """Write contents of ligand PDB to given filename"""
    with ligand_filename.open(mode="w") as flig:
        flig.write(ligand_pdb)
    logger.info(f"created file {ligand_filename}")


def convert_smiles(smiles_file, ligand_code):
    run_subprocess(
        "obabel "
        f"-ismi {smiles_file} "
        f"-opdb {ligand_code} "
        "--gen3d")


def process_input(
        pdb_code,
        ligand_code,
        protein_file,
        lig_file,
        smiles_file,
        smiles_code,
        workingdir):
    """
    Process workflow's input type and files.
    Returns ligand filename.
    """
    logger.info("Processing protein and ligand input")
    protpdbfile = workingdir / f"{pdb_code}.pdb"
    ligpdbfile = workingdir / f"{ligand_code}.pdb"

    # download protein PDB file if required
    if protein_file:
        logger.info(f"using provided protein file {protein_file}")
        protein_file = pathlib.Path(protein_file)
        protpdbfile.write_text(protein_file.read_text())
    else:
        if not protpdbfile.exists():
            logger.info("downloading protein file")
            download_pdb_file(pdb_code, protpdbfile)

    # extract ligand if required
    if lig_file:
        logger.info(f"using provided ligand file {lig_file}")
        lig_file = pathlib.Path(lig_file)
        ligpdbfile.write_text(lig_file.read_text())
    elif smiles_code:
        logger.info("converting SMILES code to PDB")
        smiles_file = pathlib.Path("ligand.smiles")
        smiles_file.write_text(smiles_code)
        convert_smiles(smiles_file, ligand_code)
    elif smiles_file:
        logger.info("converting SMILES file to PDB")
        smiles_file = pathlib.Path(smiles_file)
        convert_smiles(smiles_file, ligand_code)
    else:
        logger.info(f"extracting ligand {ligand_code} from protein PDB file")
        ligand_data = extract_ligand(protpdbfile, ligand_code)
        write_ligand_file(ligand_data, ligpdbfile)
