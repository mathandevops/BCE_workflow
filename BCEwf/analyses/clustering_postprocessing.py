import time
import logging
import multiprocessing as mp
from itertools import combinations, permutations
from functools import partial

import numpy as np
from scipy.spatial.distance import cdist
from matplotlib.pyplot import hist

import BCEwf.utils.density_peaks_clustering as dpc
from BCEwf.utils.cache import persist_to_file
from BCEwf.utils.utils import (
    run_subprocess,
    get_rot_table,
    build_dih_matrix)


logger = logging.getLogger("BCE_workflow")


def build_dihedrals_summary(dih_ndx):
    logger.info("building dihedrals summary dictionary")
    # read files contents
    ndx_data = dih_ndx.read_text().split("\n")
    # create a dictionary to get dihedral data more easily
    dih_summary = {}
    all_idx = ndx_data.index("[ ALL ]")
    for k in range(0, all_idx, 2):
        head = ndx_data[k]
        dihtype = head[2]
        dihnum = head[6:-1].strip()
        dihs = ndx_data[k + 1]
        dih_summary[int(dihnum)] = (dihtype, dihs)
    return dih_summary


def detect_rings_and_rotbonds(ligand_code, acpype_dir, aux_files_path):
    # ring detection
    logger.info("get all atoms that are part of a ring structure")
    mol2 = acpype_dir / f"{ligand_code}.mol2"
    rotrings = aux_files_path / "rotRings.pl"
    stdout, stderr = run_subprocess(f"perl {rotrings} {mol2}")
    rotrings_output_log = stdout
    rotrings_output_log = rotrings_output_log.split("\n")
    # extract ring atom numbers
    # get lines that start with 'Rings' or 'Rotable' in rotrings.pl output
    rotbonds_detected = list(
        filter(lambda x: x.startswith("Rotatable"), rotrings_output_log))
    rings_detected = list(
        filter(lambda x: x.startswith("Rings"), rotrings_output_log))
    return rings_detected, rotbonds_detected


def get_flexible_rotbonds(rotbonds_detected, dih_summary):
    flex_dihedrals = set()
    for elem in rotbonds_detected:
        rot_bond_atoms = elem[elem.index(":") + 1:]
        rot_bond_atoms = rot_bond_atoms.replace("-", "").strip()
        rot_bond_atoms = rot_bond_atoms.replace("  ", " ")
        for key, value in dih_summary.items():
            if (
                (rot_bond_atoms in value[1] or
                    rot_bond_atoms[::-1] in value[1]) and
                    value[0] == "Q"):
                flex_dihedrals = flex_dihedrals.union({key})
    return flex_dihedrals


def get_flexible_rings(rings_detected, dih_summary):
    # extract only number list from rings_detected and append to rings
    rings = []
    for elem in rings_detected:
        ring_atoms = elem[elem.index(":") + 1:].strip().split()
        ring_atoms = [int(x) for x in ring_atoms]
        rings += ring_atoms

    # find dihedrals per ring
    pmt_dict = {v[1]: k for k, v in dih_summary.items()}
    dih_per_ring = []
    if len(rings) == 3:
        dih_per_ring.append(None)
    else:
        # combine atoms in groups of 4
        comb = combinations(rings, 4)
        for elem in comb:
            permuts = permutations(elem)
            for pmt in permuts:
                pmt_str = " ".join(map(str, pmt))
                # get line index where perm is found in ndx file
                dih = pmt_dict.get(pmt_str, None)
                if dih:
                    dih_per_ring.append(dih)
    dih_per_ring = list(set(dih_per_ring))

    # select flexible rings in dih_per_ring
    flexible_rings = []
    elem = [x for x in dih_per_ring if dih_summary[x][0] in ("F", "Q")]
    if len(elem) == len(dih_per_ring):
        # accepted if ALL dihedrals found are flexible
        flexible_rings = dih_per_ring

    return flexible_rings


def select_rings_to_erase(flexible_rings, dih_summary):
    # select (redundant?) dihedral rings to erase
    ring_erase = []
    if len(flexible_rings) == 6:
        first_dih = flexible_rings[0]
        second_dih = []
        atoms_in_first = dih_summary[first_dih][1].split()
        for i in range(1, 6):
            flexring_i = flexible_rings[i]
            dihedrals = dih_summary[flexring_i][1].split()
            if (
                    (dihedrals[0] == atoms_in_first[0] and
                        dihedrals[3] == atoms_in_first[3]) or
                    (dihedrals[3] == atoms_in_first[0] and
                        dihedrals[0] == atoms_in_first[3])):
                second_dih.append(flexring_i)
        pair_dih = (first_dih, second_dih)
        ring_erase = [x for x in flexible_rings if x not in pair_dih]
    elif len(flexible_rings) == 5:
        # assuming number_of_dih_in_5rings=1 from original script
        ring_erase = flexible_rings[1:]
    return ring_erase


def select_rotdata_to_erase(pdb_code, aux_files_path):
    rot_table = get_rot_table(
        aux_files_path / "list-pdb-lig-sym-bynumberofROT.txt")
    try:
        pdb_rotdata = rot_table.get(pdb_code.lower())
    except AttributeError:
        raise AttributeError(
            f"{pdb_code} not found in file that contains "
            "information about ligand symmetries. Exiting...")
    erase = pdb_rotdata["dih_sym"].split(",")
    erase = [int(i) for i in erase]

    dihsym180 = pdb_rotdata["dih_180sym"].split(",")
    dihsym180 = [int(i) for i in dihsym180]

    return erase, dihsym180


@persist_to_file
def get_flex_dihedrals(
        ligand_code,
        pdb_code,
        angle_unit,
        dih_ndx,
        dih_dir,
        acpype_dir,
        aux_files_path):
    logger.info("getting flexible dihedrals to build matrix")

    dih_summary = build_dihedrals_summary(dih_ndx)
    rings_detected, rotbonds_detected = detect_rings_and_rotbonds(
        ligand_code,
        acpype_dir,
        aux_files_path)

    flex_dihedrals = get_flexible_rotbonds(rotbonds_detected, dih_summary)
    flexible_rings = get_flexible_rings(rings_detected, dih_summary)

    ring_erase = select_rings_to_erase(flexible_rings, dih_summary)
    rotdata_erase, dihsym180 = select_rotdata_to_erase(
        pdb_code,
        aux_files_path)

    erase = ring_erase + rotdata_erase
    erase = list(set(erase))
    if len(erase) != 1:
        erase = [x for x in erase if x != 0]

    # delete symmetrical dihedrals for better matrix calculation
    if len(erase) != 1:
        flex_dihedrals = flex_dihedrals.difference(set(erase))

    # build dihedral matrix from flex_dihedrals
    flexdihedral_values = build_dih_matrix(
        dih_dir=dih_dir,
        dih_indexes=list(flex_dihedrals),
        angle_unit=angle_unit)

    # multiply by 2 dihedrals with a 180 deg symmetry
    if not dihsym180[0] == 0:
        for d in dihsym180:
            flexdihedral_values[:, d] *= 2

    return flexdihedral_values


def compute_row_distance(mat, row):
    outrow = cdist(row, mat, dihmetric_distance)
    return outrow


def compute_dihmetric_matrix(values, procs):
    # handle mode of matrix calculation
    if procs == 0:
        logger.info("computing dihmetric matrix with SciPy")
        dihmetric_matrix = compute_with_scipy(values)
    else:
        logger.info("computing dihmetric matrix with multiprocessing")
        dihmetric_matrix = compute_with_multiprocessing(values, procs)
    return dihmetric_matrix


@persist_to_file
def compute_with_multiprocessing(values, procs):
    logger.info("computing dihMetric matrix...")

    pool = mp.Pool(procs)
    num_rows = values.shape[0]
    chunksize, extra = divmod(num_rows//5, procs)
    chunksize += extra
    dihmetric_matrix = []
    func = partial(compute_row_distance, values)

    start = time.time()
    matrix_generator = pool.imap(
        func,
        ([values[i]] for i in range(num_rows)),
        chunksize=chunksize)

    for result in matrix_generator:
        dihmetric_matrix.append(result)
    dihmetric_matrix = np.array(dihmetric_matrix).reshape(num_rows, num_rows)

    end = time.time()
    logger.info(
        "Calculation of dihMetric matrix "
        f"took {(end-start)//60} minutes")

    pool.close()
    pool.join()

    return dihmetric_matrix


@persist_to_file
def compute_with_scipy(values):
    logger.info("computing dihMetric matrix...")
    start = time.time()
    dihmetric_matrix = cdist(values, values, dihmetric_distance)
    end = time.time()
    logger.info(
        "Calculation of dihMetric matrix "
        f"took {(end-start)//60} minutes")
    return dihmetric_matrix


def dihmetric_distance(arr_a, arr_b):
    """Calculate distance between structures a and b.
    Use DihMetric definition in dihedral matrix."""
    dim = len(arr_a)
    d_ab = np.cos(arr_a - arr_b)
    d_ab = 2 * (1 - d_ab)
    return np.sqrt(d_ab.sum() / dim)


@persist_to_file
def select_cutoff(dihmetric_matrix, cutoff_pct):
    logger.info("selecting cut-off from DihMetric matrix")
    x, y = dihmetric_matrix.shape
    counts, breaks, _ = hist(dihmetric_matrix, bins="sturges")
    counts = np.sum(counts, axis=0)  # sum count values per bin
    percentages = np.cumsum(counts) * 100 / sum(counts)
    max_pct = percentages[percentages <= cutoff_pct].max()
    idx_max_pct = np.where(percentages == max_pct)[0][0]
    cluster_cutoff = breaks[idx_max_pct + 1]
    return cluster_cutoff


def extract_old_centroids(dih_dir):
    logger.info("extracting centroids from cluster.rep")
    cluster_rep = dih_dir / "cluster.rep"
    old_centroids = cluster_rep.read_text().split("\n")
    old_centroids.pop(-1)
    old_centroids = [int(line[5:]) for line in old_centroids]
    old_centroids = np.array(old_centroids)
    return old_centroids


def read_QM_energies(QM_energy_file):
    logger.info("reading QM energy file")
    QM_energies = []
    data = QM_energy_file.read_text().split("\n")[1:]
    for line in data:
        line = line[line.index("(") + 1:line.index("kcal/mol")].strip()
        line = -1 * float(line)
        QM_energies.append(line)
    return QM_energies


def define_new_centroids(
        num_dihedrals,
        cluster_cutoff,
        old_centroids_ord,
        dihmetric_matrix):
    logger.info("defining new centroids and similarity cut-off")
    if num_dihedrals == 1:
        similarity_cutoff = 0.21
        new_centroids = eliminate_centroids(
            old_centroids_ord,
            dihmetric_matrix,
            similarity_cutoff)
    elif num_dihedrals == 2:
        if np.mean(dihmetric_matrix) >= 1:
            similarity_cutoff = 0.6
            new_centroids = eliminate_centroids(
                old_centroids_ord,
                dihmetric_matrix,
                similarity_cutoff)
        else:
            similarity_cutoff = cluster_cutoff
            if cluster_cutoff >= 0.6:
                new_centroids = eliminate_centroids(
                    old_centroids_ord,
                    dihmetric_matrix,
                    similarity_cutoff)
            else:
                results = eliminate_centroids_varcutoff(
                    old_centroids_ord,
                    dihmetric_matrix,
                    similarity_cutoff,
                    change=0.05)
                similarity_cutoff = results[1]
                new_centroids = results[0]
    elif num_dihedrals == 3 or num_dihedrals == 4:
        similarity_cutoff = 0.5
        results = eliminate_centroids_varcutoff(
            old_centroids_ord,
            dihmetric_matrix,
            similarity_cutoff,
            change=-0.1)
        new_centroids = results[0]
        similarity_cutoff = results[1]
    elif num_dihedrals >= 5:
        similarity_cutoff = 0.5
        new_centroids = eliminate_centroids(
            old_centroids_ord,
            dihmetric_matrix,
            similarity_cutoff)
    return new_centroids, similarity_cutoff


def eliminate_centroids(
        centroids,
        dihmetric_matrix,
        dist_cutoff):
    for i in range(len(centroids)):
        mat = dihmetric_matrix[centroids, :][:, centroids]
        if i < mat.shape[0]:
            far_centroids = np.where(mat[i, :] >= dist_cutoff)[0]
            centroid_idx = np.concatenate(
                (far_centroids, np.where(mat[i, :] == 0)[0]), axis=0)
            if centroid_idx.size != 0:
                centroids = [centroids[idx] for idx in centroid_idx]
    return centroids


def eliminate_centroids_varcutoff(
        centroids,
        dihmetric_matrix,
        dist_cutoff,
        change):
    new_centroids = eliminate_centroids(
        centroids, dihmetric_matrix, dist_cutoff)
    new_centroids_var = eliminate_centroids(
        centroids,
        dihmetric_matrix,
        dist_cutoff + change)
    if len(new_centroids) != len(new_centroids_var):
        return list(new_centroids_var, dist_cutoff + change)
    else:
        return list(new_centroids, dist_cutoff)


def cluster_assignation(centroids, dihedrals, cutoff):
    logger.info(
        "assigning dihedrals to a cluster centroid "
        "according to the minimum distance criteria")
    frames, _ = dihedrals.shape
    centroids_len = len(centroids)
    if centroids_len == 0:
        return {"noise": []}, {"noise": 0}
    # build cluster assignment dictionary
    cluster_assign = {}
    for i in range(frames):
        distances = [
            dihmetric_distance(dihedrals[i, :], dihedrals[cent, :])
            for cent in centroids]
        min_distance = min(distances)
        if min_distance < cutoff:
            min_idx = distances.index(min_distance)
            assigned_cluster = centroids[min_idx]
        if min_distance >= cutoff:
            assigned_cluster = "noise"
        frame_list = cluster_assign.get(assigned_cluster, [])
        frame_list.append(i)
        cluster_assign[assigned_cluster] = frame_list
    # build clusters populations dictionary
    cluster_populations = {f"centroid {key}": len(val)
                           for key, val in cluster_assign.items()
                           if key != "noise"}
    noise = cluster_assign.get("noise", None)
    if not noise:
        noise = []
        cluster_assign["noise"] = noise
    cluster_populations["noise"] = len(noise)
    return cluster_assign, cluster_populations


# adaptation of postpro_clustering_dir.R
def clustering_postprocessing(
        ligand_code,
        pdb_code,
        procs,
        angle_unit,
        max_noise,
        erasing_method,
        postpro_dir,
        dih_dir,
        acpype_dir,
        aux_files_path):
    # clustering post-process: remove redundancy & adjust populations
    logger.info("Performing custering post-processing")
    dih_ndx = dih_dir / f"{ligand_code}_dihedrals_new.ndx"

    # flexdihedral values is a matrix of dihedral angles
    # column i: angles of the i-th dihedral
    flexdihedral_values = get_flex_dihedrals(
        ligand_code,
        pdb_code,
        angle_unit,
        dih_ndx,
        dih_dir,
        acpype_dir,
        aux_files_path)

    # calculate distance matrix using dihedral metric
    dihmetric_matrix = compute_dihmetric_matrix(flexdihedral_values, procs)

    # select cut-off to be used with clustering from the dihmetric matrix
    cluster_cutoff = select_cutoff(
        dihmetric_matrix=dihmetric_matrix,
        cutoff_pct=30)

    # extract old centroids
    old_centroids = extract_old_centroids(dih_dir)
    old_centroids_save = old_centroids.copy()

    logger.info(f"using cutoff = {cluster_cutoff:.2f}")

    # define old_centroids according to erasing method
    logger.info(
        "define old_centroids according "
        f"to erasing method \"{erasing_method}\"")
    if erasing_method == "gamma":
        # compute density and delta
        densities, deltas = dpc.compute_quantities(
            dihmetric_matrix, cluster_cutoff)
        # compute gamma
        gamma = dpc.compute_gamma(densities, deltas)
        little_gamma = np.array([gamma[k] for k in old_centroids])
        old_centroids_ord = old_centroids[np.argsort(little_gamma)[::-1]]

    elif erasing_method == "QM_energy":
        QM_energies = read_QM_energies()
        old_centroids_ord = old_centroids[np.argsort(QM_energies)]

    # eliminate centroids that are very close
    new_centroids, similarity_cutoff = define_new_centroids(
        num_dihedrals=flexdihedral_values.shape[0],
        cluster_cutoff=cluster_cutoff,
        old_centroids_ord=old_centroids_ord,
        dihmetric_matrix=dihmetric_matrix)
    N = len(new_centroids)
    logger.info(f"new_centroids length: {N}")
    logger.info(f"similarity_cutoff: {similarity_cutoff}")

    final_cutoff = cluster_cutoff
    noise = max_noise + 1
    while noise > max_noise:
        # cluster_assign:
        #   keys: cluster centroid
        #   values: structures that belong to that cluster
        # cluster_populations:
        #   keys: cluster centroid
        #   values: number of structures that belong to cluster
        cluster_assign, cluster_populations = cluster_assignation(
            new_centroids, flexdihedral_values, final_cutoff)

        noise = cluster_populations.get("noise", 0)
        logger.info(f"Current amount of noise: {noise}")

        final_cutoff += 0.1

    logger.info(f"the final cutoff value is {final_cutoff:.2f}")
    logger.info(f"final noise: {noise}")

    # write output files
    if not postpro_dir.exists():
        postpro_dir.mkdir()
    write_individual_clusterfiles(cluster_assign, postpro_dir)
    write_cluster_information(
        cluster_populations,
        old_centroids_save,
        new_centroids,
        cluster_cutoff,
        final_cutoff,
        similarity_cutoff,
        postpro_dir)


def write_individual_clusterfiles(cluster_assignments, postpro_dir):
    logger.info("writing cluster files for all centroids snapshots")
    for key, val in cluster_assignments.items():
        filename = f"CLUSTER_centroid{key}.txt"
        logger.info(f"writing file {filename}")
        filepath = postpro_dir / filename
        with filepath.open("w") as f:
            f.write(f"[ CLUSTER CENTROID {key} ]")
            for elem in val:
                f.write(f"{elem}\n")


def write_cluster_information(
        cluster_populations,
        old_centroids,
        new_centroids,
        initial_cutoff,
        final_cutoff,
        similarity_cutoff,
        postpro_dir):
    filename = "cluster_info.txt"
    logger.info(f"writing clusters information file {filename}")
    filepath = postpro_dir / filename
    with filepath.open("w") as f:
        f.write("[ GENERAL INFORMATION ]")
        f.write(f"old_centroids: {old_centroids}")
        f.write(f"new_centroids: {new_centroids}")
        f.write(f"initial cut-off: {initial_cutoff}")
        f.write(f"final cut-off: {final_cutoff}")
        f.write(f"similarity cut-off: {similarity_cutoff}")
        f.write("[ CLUSTER POPULATIONS ]")
        f.write("centroid num. | population")
        noise_key = "noise"
        noise_val = cluster_populations.pop(noise_key)
        # write everything except noise
        for key, val in cluster_populations.items():
            f.write(f"{key:>13}   {val:>10}")
        # write noise at the end
        f.write(f"{noise_key.upper():>8}   {noise_val:>10}")
