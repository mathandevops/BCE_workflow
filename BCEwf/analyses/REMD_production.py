"""Prepare, perform and post-process REMD production simulations"""
import logging
import subprocess
import shlex

from re import search

import numpy as np

from BCEwf.utils.utils import run_subprocess

logger = logging.getLogger("BCE_workflow")


def create_gmx_runfile(
        length,
        int_temp,
        ensemble,
        ligand_code,
        gmx_run_file,
        rundir,
        mode=2):
    # ns to steps ( length to ps / 2ps timestep)
    md_time = int(length * 1000 / 0.002)

    # define default values for tc_grps and tc_grps_wat
    if ensemble.lower() == "npt":
        tc_grps = "Other"
        tc_grps_wat = "Water_and_ions"
    elif ensemble.lower() == "nvt":
        tc_grps = "Other_Ions"
        tc_grps_wat = "Water"
    # change Berendsen group if no ions added, depending on mode
    grep_cmd = f"grep 'No ions to add' {ligand_code}_genion.log"
    stdout, stderr = run_subprocess(grep_cmd, raise_exc=False)
    if stdout != "":
        if mode == 1:
            tc_grps = "Other"
        if mode == 2:
            tc_grps_wat = "Water"

    gmx_run_file_data = gmx_run_file[ensemble.lower()]
    gmx_run_file_data = gmx_run_file_data.format(
        REMD_temp=int_temp,
        md_time=md_time,
        tc_grps=tc_grps,
        tc_grps_wat=tc_grps_wat)
    run_file = rundir / "gmx_run_file.mdp"
    run_file.write_text(gmx_run_file_data)


def prep_topfiles(infile, outfile):
    infile_contents = infile.read_text().split("\n")
    # add 2 to skip header lines
    start = infile_contents.index("[ atoms ]") + 2
    # subtract 1 to avoid empty line
    end = infile_contents.index("[ bonds ]") - 1
    atoms = infile_contents[start:end]
    atoms = [line[:line.index(";")] for line in atoms]
    infile_contents[start:end] = atoms
    outfile.write_text("\n".join(infile_contents))


def check_postprocessing_output(length, postprocess_output):
    number_frames = length * 1000  # ns to ps
    frames = fr"frame\s+{number_frames}"
    match = search(frames, postprocess_output.lower())
    if not match:
        raise Exception(
            "Final trajectory is smaller than expected! "
            "Please check output and log files")


def select_gro_file(ligand_code, gmx_dir):
    # see if gro file exists, else use "conf_noions" file
    try:
        gro = gmx_dir / f"{ligand_code}_wat.gro"
        assert gro.exists()
    except AssertionError:
        gro = gmx_dir / f"{ligand_code}_conf_noions.gro"
    return gro


def check_rundir_exists(rundir):
    if not rundir.exists():
        logger.info(f"creating new directory {rundir}")
        rundir.mkdir()


def prepare_top_hrex(
        ligand_code,
        rep,
        tlow,
        temp,
        reps,
        procs,
        top,
        length,
        ensemble,
        gmx_run_file,
        job_prefix,
        rundir):
    logger.info("preparing Gromacs TOP Hamiltonian REMD production topologies")

    # plumed.dat file required (even if it is empty)
    plumed_file = rundir / "plumed.dat"
    plumed_file.touch()

    topology_contents = top.read_text()
    topology_lines = topology_contents.split("\n")
    include_line = [
        line for line in topology_lines
        if "GMX" in line and line.strip().startswith("#")][0]
    logger.debug(f"include_line: {include_line}")
    # get filename
    include_file = include_line.strip().split(" ")[-1]
    # remove quote characters
    include_file = top.parent / include_file.replace("\"", "")
    include_file_contents = include_file.read_text()

    # replace #include "[ligand]_GMX.itp" with contents of .itp file
    topology_contents = topology_contents.replace(
        include_line, include_file_contents)

    hrex_top = rundir / f"top.hrex{rep}.top"
    hrex_top.write_text(topology_contents)

    # Process topology:
    # 1) Choose "HOT" region/atoms adding "_" to them
    # in the [ atoms ] section of the topology
    # Adding the "_" to all atoms of the peptide is equivalent to
    # REST2 (the entire solute is scaled).

    tophrex_lambda = rundir / f"top.hrex{rep}.lambda.top"
    prep_topfiles(hrex_top, tophrex_lambda)

    # Process topology:
    # 2) Scaling Hamiltonian terms using plumed partial_tempering script.
    if not procs:
        slurm_procs = reps
    else:
        slurm_procs = procs
    plumed_cmd = (
        f"{job_prefix} {slurm_procs} plumed partial_tempering {tlow / temp:.1f} "
        f"< {tophrex_lambda}")
    stdout, stderr = run_subprocess(
        plumed_cmd,
        logfile=rundir / f"top.hrex{rep}.lambda.hot.top")

    # building gromacs run file
    create_gmx_runfile(
        length,
        tlow,
        ensemble,
        ligand_code,
        gmx_run_file,
        rundir,
        mode=2)


def prepare_REMD_production(
        thigh,
        tlow,
        length,
        ligand_code,
        gmx_dir,
        eq_steps,
        reps,
        procs,
        ensemble,
        gmx_run_file,
        maxwarn,
        job_prefix,
        gmx_exec,
        use_hrex):
    top = gmx_dir / f"{ligand_code}_GMX.mod.top"
    ndx_file = gmx_dir / f"{ligand_code}.ndx"
    for rep in range(reps):
        # define files inside replica directory
        rundir = gmx_dir / f"simulation_rep{rep}"
        eqdir = gmx_dir / f"equilib_rep{rep}_step{eq_steps}"
        run_file = rundir / "gmx_run_file.mdp"
        sim_tpr_file = rundir / "topol.tpr"
        sim_gro_file = eqdir / "confout.gro"
        mdout_file = rundir / "mdout.mdp"
        sim_grompp_log = rundir / "sim_gromp.log"
        temp = tlow * np.exp((rep - 1) *
                             np.log(thigh / tlow) / (reps - 1))
        int_temp = int(temp)

        check_rundir_exists(rundir)
        # branching between REMD and HREX
        # HREX
        if use_hrex:
            logger.info("using Hamiltonian Replica Exchange")
            prepare_top_hrex(
                ligand_code,
                rep,
                tlow,
                temp,
                reps,
                procs,
                top,
                length,
                ensemble,
                gmx_run_file,
                job_prefix,
                rundir)
        else:
            # REMD
            logger.info("using Temperature Replica Exchange")
            logger.info(
                f"preparing replica: {rep} "
                f"at temperature: {int_temp}")
            create_gmx_runfile(
                length,
                int_temp,
                ensemble,
                ligand_code,
                gmx_run_file,
                rundir,
                mode=1)

        logger.info(
            "preparing Gromacs TPR file for MD")
        # en HREX cambia {top} q sale de plumed partial_temperating cmd
        gmx_cmd = (
            # f"srun -n 1 gmx_mpi grompp "
            f"{gmx_exec} grompp "
            f"-f {run_file} "
            f"-c {sim_gro_file} "
            f"-p {top} -n {ndx_file} "
            f"-o {sim_tpr_file} "
            f"-po {mdout_file} "
            f"-maxwarn {maxwarn}")
        stdout, stderr = run_subprocess(
            gmx_cmd, logfile=sim_grompp_log)


def postprocess_REMD_production(
        ligand_code,
        length,
        gmx_dir,
        gmx_exec,
        postp_rep):
    gro = select_gro_file(ligand_code, gmx_dir)
    sim0_tpr_file = gmx_dir / \
        f"simulation_rep{postp_rep}" / "topol.tpr"
    ret_pdb_file = gmx_dir / f"{ligand_code}.ret.pdb"
    trjconv_log = gmx_dir / "trjconv.retPdb.log"
    rundir = gmx_dir / f"simulation_rep{postp_rep}"
    mdlog = rundir / "md.log"
    sim_tpr_file = rundir / "topol.tpr"
    sim_out_xtc_file_raw = rundir / "traj_comp.xtc"  # used to be traj.xtc
    sim_out_xtc_file = rundir / f"{ligand_code}.imaged.xtc"
    sim_out_xtc_rot_file = rundir / f"{ligand_code}.imaged.rot.xtc"
    trjconv_image_log = rundir / "trjconv_image.log"
    trjconv_rot_log = rundir / "trjconv_rot.log"

    logger.info("extracting ligand pdb from MD system")
    gmx_cmd = (
        f"{gmx_exec} trjconv "
        f"-f {gro} -s {sim0_tpr_file} "
        f"-o  {ret_pdb_file} "
        "-ur compact -pbc atom")
    stdout, stderr = run_subprocess(
        gmx_cmd,
        stdin=subprocess.PIPE,
        msg=b"0",
        logfile=trjconv_log)

    logger.info("de-multiplexing REMD - HREX trajectory")
    subprocess.call(shlex.split(f"demux.pl {mdlog}"))

    # Image trajectory (moving ligand to center of the water box)
    logger.info(f"imaging trajectory simulation_rep{postp_rep}")
    gmx_cmd = (
        f"{gmx_exec} trjconv "
        f"-s {sim_tpr_file} "
        f"-f {sim_out_xtc_file_raw} "
        f"-o {sim_out_xtc_file} "
        "-pbc mol -center -ur compact")
    stdout, stderr = run_subprocess(
        gmx_cmd,
        stdin=subprocess.PIPE,
        msg=b"1 0",
        logfile=trjconv_image_log)

    # Removing rotation
    gmx_cmd = (
        f"{gmx_exec} trjconv "
        f"-s {sim_tpr_file} "
        f"-f {sim_out_xtc_file} "
        f"-o {sim_out_xtc_rot_file} "
        "-fit rot+trans")
    stdout, stderr = run_subprocess(
        gmx_cmd,
        stdin=subprocess.PIPE,
        msg=b"1 0",
        logfile=trjconv_rot_log)

    check_postprocessing_output(length, stdout+stderr)


def run_REMD_production(
        reps,
        procs,
        replex,
        ligand_code,
        use_hrex,
        job_prefix,
        gmx_mpi_exec,
        gmx_dir):
    sim_out_file = gmx_dir / f"{ligand_code}-simout"
    sim_out_xtc_raw = gmx_dir / f"{ligand_code}.xtc"
    remd_range = " ".join(
        [f"GMX/simulation_rep{rep}" for rep in range(reps)])
    if not procs:
        slurm_procs = reps
    else:
        slurm_procs = procs
    if use_hrex:
        mdrun_cmd = (
            f"{job_prefix} {slurm_procs} {gmx_mpi_exec} mdrun "
            "-hrex -plumed plumed.dat "
            f"-multidir {remd_range} -o {sim_out_file} "
            f"-replex {replex} "
            f"-x {sim_out_xtc_raw}")
    else:
        mdrun_cmd = (
            f"{job_prefix} {slurm_procs} {gmx_mpi_exec} mdrun "
            f"-v -multidir {remd_range} -o {sim_out_file} "
            f"-replex {replex}")
    stdout, stderr = run_subprocess(mdrun_cmd)


def REMD_production(
        tlow,
        thigh,
        length,
        ligand_code,
        reps,
        procs,
        ensemble,
        maxwarn,
        job_prefix,
        gmx_exec,
        gmx_mpi_exec,
        eq_steps,
        postp_rep,
        replex,
        use_hrex,
        gmx_dir,
        gmx_run_file):
    # prepare runs
    logger.info("Performing REMD production")
    logger.info(f"temperature range: {tlow} - {thigh}")

    logger.info("preparing REMD production runs")
    prepare_REMD_production(
        thigh,
        tlow,
        length,
        ligand_code,
        gmx_dir,
        eq_steps,
        reps,
        procs,
        ensemble,
        gmx_run_file,
        maxwarn,
        job_prefix,
        gmx_exec,
        use_hrex)

    # run REMD production
    logger.info("running production MDs in REMD...")
    run_REMD_production(
        reps,
        procs,
        replex,
        ligand_code,
        use_hrex,
        job_prefix,
        gmx_mpi_exec,
        gmx_dir)

    # post-processing of REMD trajectories
    logger.info("post-processing Gromacs REMD trajectories generated")
    postprocess_REMD_production(
        ligand_code,
        length,
        gmx_dir,
        gmx_exec,
        postp_rep)
