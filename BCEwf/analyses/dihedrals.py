import logging
import subprocess
import re
from itertools import combinations, permutations

import numpy as np
from matplotlib.pyplot import hist

from BCEwf.utils.utils import (
    run_subprocess, check_cluster_percentage, build_dih_matrix)

logger = logging.getLogger("BCE_workflow")


def generate_gmx_ndx(ligand_code, dih_dir, gmx_dir):
    gmx_itp_file = gmx_dir / f"{ligand_code}_GMX.itp"
    dih_ndx_file = dih_dir / f"{ligand_code}_dihedrals.ndx"

    # copy contents from itp to ndx file
    dihedral_count = 1
    final_all = "[ ALL ]\n"
    if dih_ndx_file.exists():
        dih_ndx_file.unlink()
    f_ndx = dih_ndx_file.open(mode="x")

    # read gmx .itp file contents and get only proper dihedrals segment
    itp_contents = gmx_itp_file.read_text()
    start_ndx = itp_contents.index("[ dihedrals ] ; propers")
    end_ndx = itp_contents.index("[ dihedrals ] ; impropers")
    itp_contents = itp_contents[start_ndx:end_ndx]
    itp_contents = itp_contents.split("\n")
    itp_contents = [line for line in itp_contents if
                    line and not line.startswith((";", "["))]
    for line in itp_contents:
        if "H" in line:
            continue
        else:
            d1, d2, d3, d4, _ = line.split(maxsplit=4)
            f_ndx.write(f"[ PDIH{dihedral_count} ]\n")
            pdih_line = f"{d1} {d2} {d3} {d4}\n"
            f_ndx.write(pdih_line)
            final_all += pdih_line
            dihedral_count += 1

    with dih_ndx_file.open(mode="a") as f_ndx:
        f_ndx.write(final_all)
    return dihedral_count


def extract_cluster_conformations(gmx_dir, dih_dir):
    cluster_log = gmx_dir / "cluster.log"
    cluster_rep = dih_dir / "cluster.rep"

    data = cluster_log.read_text().split("\n")
    data = [s for s in data if re.match(r"^\s+\d+", s)]

    def clean_line(line):
        line = line.split("|")
        line = [s.strip().split() for s in line[:-1]]
        num = line[0][0]  # cl.
        centroid = line[2][0]  # middle
        return num, centroid
    data = [clean_line(s) for s in data]
    data = [f"CL{t[0]}: {t[1]}" for t in data]

    cluster_rep.write_text("\n".join(data))


def calculate_dihedral_angles(
        ligand_code,
        postp_rep,
        dihedral_count,
        dih_dir,
        gmx_dir,
        job_prefix,
        gmx_mpi_exec):
    postp_rep_dir = gmx_dir / f"simulation_rep{postp_rep}"
    # <dihedral_count> adds 1 at the end of the loop
    imaged_rot_xtc_file = postp_rep_dir / \
        f"{ligand_code}.imaged.rot.xtc"
    dih_ndx_file = dih_dir / f"{ligand_code}_dihedrals.ndx"
    for dih_count in range(1, dihedral_count):
        angle_xvg = dih_dir / f"angle{dih_count}.xvg"
        dih_xvg = dih_dir / f"dih{dih_count}.xvg"

        logger.info(f"computing PDIH{dih_count} dihedral angle")
        angle_cmd = (
            f"{job_prefix} 1 {gmx_mpi_exec} angle "
            f"-f {imaged_rot_xtc_file} "
            f"-n {dih_ndx_file} "
            f"-od {angle_xvg} "
            "-type dihedral "
            f"-ov {dih_xvg} ")
        stdout, stderr = run_subprocess(
            angle_cmd,
            stdin=subprocess.PIPE,
            msg=bytes(f"PDIH{dih_count}", "utf-8"))


def get_cluster_number(cluster_log, clModels, clPercentage, length):
    if not cluster_log.exists():
        logger.info("output file cluster.log not found, "
                    f"working with {clModels} clusters.")
        num_clusters = clModels
    else:
        _, num_clusters = check_cluster_percentage(
            cluster_log,
            clModels,
            clPercentage,
            length)
        logger.info(f"working with {num_clusters} "
                    "clusters (from cluster.log)")
    return num_clusters


def dihedrals(
        ligand_code,
        clModels,
        clPercentage,
        postp_rep,
        length,
        angle_unit,
        gmx_dir,
        dih_dir,
        acpype_dir,
        job_prefix,
        gmx_exec,
        gmx_mpi_exec,
        aux_files_path):
    logger.info("Computing dihedral angles")

    if not dih_dir.exists():
        dih_dir.mkdir()

    # num_clusters = get_cluster_number(
    #     gmx_dir / "cluster.log",
    #     clModels,
    #     clPercentage,
    #     length)

    logger.info(
        "generating Gromacs index file (ndx) "
        "with all dihedrals taken from itp topology")
    # create {ligand_code}_dihedrals.ndx file
    dihedral_count = generate_gmx_ndx(ligand_code, dih_dir, gmx_dir)

    # extract information from cluster conformations (from cluster.log)
    logger.info(
        "extracting information from cluster conformations "
        "and creating clusters.rep")
    extract_cluster_conformations(gmx_dir, dih_dir)

    logger.info("calculating dihedral angles")
    calculate_dihedral_angles(
        ligand_code,
        postp_rep,
        dihedral_count,
        dih_dir,
        gmx_dir,
        job_prefix,
        gmx_mpi_exec)

    logger.info("classifying dihedrals")
    angles_class, ring_class = dihedral_classification(
        ligand_code,
        angle_unit,
        acpype_dir,
        aux_files_path,
        dih_dir)

    logger.info("writing new dihedrals ndx file")
    write_new_ndx(angles_class, ring_class, ligand_code, dih_dir)


def get_angle_origins(dihedral_angles):
    logger.debug("creating angles histogram")
    m = dihedral_angles.shape[1]
    hist_data = [hist(dihedral_angles[:, j], bins="sturges")[0:2]
                 for j in range(m)]
    histogram = [val[0] for val in hist_data]
    bins = [val[1] for val in hist_data]

    origin = np.full(m, fill_value=None)
    for i in range(m):
        hist_i = histogram[i]
        # if 0 not in histogram
        if hist_i.min() != 0:
            # get index of minimum value in hist_i
            k = np.where(hist_i == hist_i.min())[0][0]
        elif hist_i[0] == 0:
            # if first value in histogram is zero, use 0 as index
            k = 0
        else:
            # get index of middle value in histogram that is zero
            k = np.where(hist_i == 0)[0].max() // 2
        # add to origin the k-th element in bins
        # it corresponds to the minimum value in histogram
        origin[i] = bins[i][k]
    return np.array(origin)


def changing_angles_domain(origin, dihedral_angles):
    n, m = dihedral_angles.shape
    selected_idxs = np.where(origin != -180)[0]
    for j in selected_idxs:
        for i in range(n):
            if dihedral_angles[i, j] < origin[j]:
                dihedral_angles[i, j] += 360
    return dihedral_angles


def get_atoms_in_ring(ligand_code, aux_files_path, acpype_dir, dih_dir):
    mol2 = acpype_dir / f"{ligand_code}.mol2"
    rotrings = aux_files_path / "rotRings.pl"
    stdout, stderr = run_subprocess(
        f"perl {rotrings} {mol2}",
        logfile=dih_dir / "rotrings_output.log")
    rotrings_output_log = stdout + stderr
    rotrings_output_log = rotrings_output_log.split("\n")
    # extract ring atom numbers
    rings = []
    # get all lines that start with 'Rings' in rotrings.pl output
    rings_detected = [x for x in rotrings_output_log if x.startswith("Rings")]
    for elem in rings_detected:
        # extract only number list from that line and append to rings list
        ring_atoms = elem[elem.index(":") + 1:].strip().split()
        ring_atoms = [int(i) for i in ring_atoms]
        rings.append(ring_atoms)
    return rings


def get_dihedral_dict(dih_ndx):
    dih_ndx_data = dih_ndx.read_text().split("\n")
    # get everything under [ ALL ]
    all_index = dih_ndx_data.index("[ ALL ]")
    dih_ndx_data = dih_ndx_data[all_index + 1:]
    dih_ndx_data.pop(dih_ndx_data.index(""))
    # create a dictionary with the atoms data and the index
    dih_ndx_data_dict = {i: dih_ndx_data.index(i) for i in dih_ndx_data}
    return dih_ndx_data_dict


def dihedral_classification(
        ligand_code,
        angle_unit,
        acpype_dir,
        aux_files_path,
        dih_dir):
    # adaptation of dihclass_dir.R
    logger.info("running dihedral angles and rings classification")
    dih_ndx = dih_dir / f"{ligand_code}_dihedrals.ndx"

    logger.debug(f"building dihedral angles matrix in {angle_unit}")
    dihedral_angles = build_dih_matrix(dih_dir=dih_dir, angle_unit=angle_unit)

    logger.debug("selecting new origins to re-center histogram")
    origin = get_angle_origins(dihedral_angles)

    logger.debug("changing angles domain")
    dihedral_angles = changing_angles_domain(origin, dihedral_angles)

    # angles are classified using standard deviation
    # if std<14 return Rigid, else return Flexible
    logger.info("classifying dihedral angles")
    n, m = dihedral_angles.shape
    angles_class = []
    for j in range(m):
        std_j = dihedral_angles[:, j].std()
        if std_j < 14:
            angles_class.append("Rigid")
        else:
            angles_class.append("Flexible")
    # remove dihedral_angles var to free space

    # ring detection
    logger.debug("get all atoms that are part of a ring structure")
    rings = get_atoms_in_ring(ligand_code, aux_files_path, acpype_dir, dih_dir)

    # this dictionary will be used to look for the dihedral number
    # when a certain dihedral combination happens to belong to a ring
    dih_ndx_data_dict = get_dihedral_dict(dih_ndx)
    ring_class = [False] * len(dih_ndx_data_dict)

    logger.info("classifying rings in dihedrals")
    logger.debug("looking for all dihedral sequences that belong to a ring")
    for atoms_in_ring in rings:
        if len(atoms_in_ring) == 3:
            # combine atoms in groups of 2
            comb_number = 2
        else:
            # combine atoms in groups of 4
            comb_number = 4
        comb = combinations(atoms_in_ring, comb_number)
        for elem in comb:
            permuts = permutations(elem)
            for pmt in permuts:
                k = dih_ndx_data_dict.get(pmt, None)
                if k:
                    ring_class[k] = True

    return angles_class, ring_class


def write_new_ndx(angles_class, ring_class, ligand_code, dih_dir):
    ndx_file = dih_dir / f"{ligand_code}_dihedrals.ndx"
    new_ndx = dih_dir / f"{ligand_code}_dihedrals_new.ndx"

    prefix_mapping = {
        "Flexible": "F",
        "Flexible Ring": "Q",
        "Rigid": "R",
        "Rigid Ring": "O"
    }

    logger.info("writing new index file with angle classification")
    f_ndx = ndx_file.open("r")
    if new_ndx.exists():
        new_ndx.unlink()
    f_ndx_new = new_ndx.open(mode="x")

    while True:
        ndx_line = f_ndx.readline()
        if not ndx_line:
            break
        elif (
                ndx_line[0].isnumeric() or
                ndx_line.startswith("[ ALL ]")):
            f_ndx_new.write(ndx_line)
        else:
            # get dihedral number
            dih_number = [c for c in ndx_line if c.isnumeric()]
            # subtract 1 from dihedral number to match list index
            dih_number = int("".join(dih_number)) - 1
            # get dihedral class
            class_line = angles_class[dih_number]
            if len(ring_class) < dih_number:
                class_line += " Ring"
            # write line to new index file
            class_prefix = prefix_mapping[class_line]
            f_ndx_new.write(f"[ {class_prefix}DIH{dih_number} ]\n")
    f_ndx.close()
    f_ndx_new.close()
