"""Perform ligand parametrization using ACPYPE"""

import logging
from re import compile

from BCEwf.utils.utils import run_subprocess, get_charge

logger = logging.getLogger("BCE_workflow")


def remove_hydrogens(ligandHmin_filename):
    data = ligandHmin_filename.read_text().split("\n")
    data = [line for line in data
            if line.startswith(("ATOM", "HETATM", "TER"))]
    output = ""
    Hpatt = compile(r"^\dH|H|OXT")
    for line in data:
        atom = line[12:16].strip()
        if Hpatt.search(atom):
            output += line
    return output


def check_acpype_output(acpype_dir):
    sqmout = acpype_dir / "sqm.out"
    logger.debug(f"sqm.out path: {sqmout}")
    if not sqmout.exists():
        raise FileNotFoundError(
            "Error in parameterization step (Antechamber -- ACPYPE). "
            f"{sqmout} not found. Please check log files.")
    else:
        with sqmout.open(mode="r") as fsqm:
            sqm = fsqm.read()
        if "calculation completed" not in sqm.lower():
            raise Exception(
                "Error in parameterization step (Antechamber -- ACPYPE). "
                "Calculation not completed. Please check log files.")


def ligand_parametrization(
        ph,
        charge,
        ligand_code,
        acpype_dir,
        workingdir,
        acpype_path="acpype.py"):
    """Parametrize ligand, create structure and topology files"""
    logger.info("Performing ligand parametrization")

    ligandH_filename = workingdir / f"{ligand_code}.H.pdb"
    ligandHmin_filename = workingdir / f"{ligand_code}.H.min.pdb"
    ligandHmin_logfile = workingdir / f"{ligand_code}.H.min.log"
    ligandmin_filename = workingdir / f"{ligand_code}.min.pdb"

    logger.info(f"adding protons at pH {ph} with OpenBabel")
    param_cmd = (
        f"obabel -ipdb {ligand_code}.pdb "
        f"-opdb -O{ligandH_filename} "
        f"-h -p{ph}")
    stdout, stderr = run_subprocess(param_cmd)

    logger.info("minimizing the energy of the ligand with protons")
    param_cmd = (
        "obminimize -sd -c 1e-10 "
        f"-ipdb {ligandH_filename} "
        f"-opdb {ligandHmin_filename}")
    stdout, stderr = run_subprocess(
        param_cmd, raise_exc=False, logfile=ligandHmin_logfile)

    # exception handling for obminimize
    if (
            "usage" in stdout.lower() or
            "explosion" in stdout.lower() or
            "cannot" in stdout.lower() or
            "could not" in stdout.lower()):
        output = stdout + stderr
        raise Exception(output)

    param_cmd = (
        "egrep \"ATOM|HETATM|CONECT|MASTER|COMPND|AUTHOR\" "
        f"{ligandHmin_logfile}")
    run_subprocess(
        param_cmd, logfile=ligandHmin_filename)

    logger.info("removing hydrogen atoms from minimized ligand")
    output = remove_hydrogens(ligandHmin_filename)
    with ligandmin_filename.open(mode="w") as f:
        f.write(output)

    if charge is None:
        logger.info("no charge given as argument, computing charge")
        charge = get_charge(ligandH_filename)
    logger.debug(f"ligand charge: {charge}")

    logger.info("running parametrization with ACPYPE")
    acpype_cmd = (
        f"python {acpype_path} -d "
        f"-i {ligandHmin_filename} "
        f"-b {ligand_code} "
        f"-n {charge}")
    run_subprocess(acpype_cmd)
    check_acpype_output(acpype_dir)
