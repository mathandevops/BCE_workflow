#!/usr/bin/env python
"""
Bioactive Conformational Ensemble (BCE) Workflow

REQUIRED ENVIRONMENT:
OpenBabel
amber/14
gromacs/5.1.4
gromacs-plumed-hrex/4.6.7 (if using HREX)
Python/3.6.4-foss-2018a (Python 3.6+)
"""

import sys

from BCEwf.analyses.ligand_extraction import process_input
from BCEwf.analyses.ligand_parametrization import ligand_parametrization
from BCEwf.analyses.REMD_equilibration import REMD_equilibration
from BCEwf.analyses.REMD_production import REMD_production
from BCEwf.analyses.clustering import clustering
from BCEwf.analyses.dihedrals import dihedrals
from BCEwf.analyses.clustering_postprocessing import clustering_postprocessing
from BCEwf.analyses.gaussian_optimization import gaussian_energy_optimization

from BCEwf.parse_args import parse_args
from BCEwf.utils import directory_utils
from BCEwf.utils.config import method_header, wf_steps, gmx_run_file
from BCEwf.utils.logs import create_logger


def main():
    # parse arguments
    args = parse_args(args=sys.argv[1:])
    # create logger
    logger = create_logger(args.verbose)
    # get paths
    (
        workingdir,
        aux_files_path,
        acpype_path,
        acpype_dir,
        gmx_dir,
        clusters_parent,
        dih_dir,
        gauss_dir,
        postpro_dir
    ) = directory_utils.get_paths(
        args.workingdir,
        args.ligand_code,
        args.use_given_workingdir,
        args.force,
        args.acpype_path)

    # gromacs and job scheduling options
    if args.use_slurm:
        job_prefix = "srun -n"
        if args.gmx_exec == "gmx_mpi":
            args.gmx_exec = "srun -n 1 gmx_mpi"
    elif args.use_compss:
        # job_prefix = ""
        raise NotImplementedError(
            "PyCompss job scheduling not implemented (yet)")
    else:
        job_prefix = ""
    gmx_mpi_exec = "gmx_mpi"

    # define arguments for workflow's steps
    ligand_extraction_args = dict(
        pdb_code=args.pdb_code,
        ligand_code=args.ligand_code,
        protein_file=args.protein_file,
        lig_file=args.lig_file,
        smiles_file=args.smiles_file,
        smiles_code=args.smiles_code,
        workingdir=workingdir)

    ligand_parametrization_args = dict(
        ph=args.ph,
        charge=args.charge,
        ligand_code=args.ligand_code,
        acpype_dir=acpype_dir,
        workingdir=workingdir,
        acpype_path=acpype_path)

    REMD_equilibration_args = dict(
        boxtype=args.boxtype,
        boxsize=args.boxsize,
        ligand_code=args.ligand_code,
        maxwarn=args.maxwarn,
        job_prefix=job_prefix,
        gmx_exec=args.gmx_exec,
        gmx_mpi_exec=gmx_mpi_exec,
        procs=args.procs,
        thigh=args.thigh,
        tlow=args.tlow,
        reps=args.reps,
        eq_steps=args.eq_steps,
        remd_eq_dt=args.REMDeq_dt,
        gmx_nsteps=args.gmx_nsteps,  # 10000
        acpype_dir=acpype_dir,
        gmx_dir=gmx_dir)

    REMD_production_args = dict(
        tlow=args.tlow,
        thigh=args.thigh,
        length=args.length,
        ligand_code=args.ligand_code,
        reps=args.reps,
        procs=args.procs,
        ensemble=args.ensemble,
        maxwarn=args.maxwarn,
        job_prefix=job_prefix,
        gmx_exec=args.gmx_exec,
        gmx_mpi_exec=gmx_mpi_exec,
        eq_steps=args.eq_steps,
        postp_rep=args.postp_rep,
        replex=args.replex,
        use_hrex=args.use_hrex,
        gmx_dir=gmx_dir,
        gmx_run_file=gmx_run_file)

    clustering_args = dict(
        cluster=args.cluster,
        postp_rep=args.postp_rep,
        procs=args.procs,
        length=args.length,
        boxtype=args.boxtype,
        boxsize=args.boxsize,
        ligand_code=args.ligand_code,
        clMethod=args.clMethod,
        clIncrement=args.clIncrement,
        clModels=args.clModels,
        clPercentage=args.clPercentage,
        gmx_dir=gmx_dir,
        job_prefix=job_prefix,
        gmx_exec=args.gmx_exec,
        gmx_mpi_exec=gmx_mpi_exec,
        clusters_parent=clusters_parent,
        aux_files_path=aux_files_path)

    dihedrals_args = dict(
        ligand_code=args.ligand_code,
        clModels=args.clModels,
        clPercentage=args.clPercentage,
        postp_rep=args.postp_rep,
        length=args.length,
        angle_unit=args.angle_unit,
        gmx_dir=gmx_dir,
        dih_dir=dih_dir,
        acpype_dir=acpype_dir,
        job_prefix=job_prefix,
        gmx_exec=args.gmx_exec,
        gmx_mpi_exec=gmx_mpi_exec,
        aux_files_path=aux_files_path)

    clustering_postprocessing_args = dict(
        ligand_code=args.ligand_code,
        pdb_code=args.pdb_code,
        procs=args.procs,
        angle_unit=args.angle_unit,
        max_noise=args.max_noise,
        erasing_method=args.erasing_method,
        postpro_dir=postpro_dir,
        dih_dir=dih_dir,
        acpype_dir=acpype_dir,
        aux_files_path=aux_files_path)

    gaussian_optimization_args = dict(
        ligand_code=args.ligand_code,
        charge=args.charge,
        clModels=args.clModels,
        clPercentage=args.clPercentage,
        length=args.length,
        reps=args.reps,
        procs=args.procs,
        gauss_dir=gauss_dir,
        workingdir=workingdir)

    # workflow execution
    # workflows' main methods and corresponding arguments
    pipeline = [
        (
            process_input,
            ligand_extraction_args,
            "PROCESSING INPUT",
        ),
        (
            ligand_parametrization,
            ligand_parametrization_args,
            "LIGAND PARAMETRIZATION",
        ),
        (
            REMD_equilibration,
            REMD_equilibration_args,
            "REMD EQUILIBRATION",
        ),
        (
            REMD_production,
            REMD_production_args,
            "REMD PRODUCTION",
        ),
        (
            clustering,
            clustering_args,
            "CLUSTERING",
        ),
        (
            dihedrals,
            dihedrals_args,
            "DIHEDRALS CALCULATION",
        ),
        (
            clustering_postprocessing,
            clustering_postprocessing_args,
            "CLUSTERING",
        ),
        (
            gaussian_energy_optimization,
            gaussian_optimization_args,
            "GAUSSIAN ENERGY OPTIMIZATION",
        )]

    # get index of partial execution step
    step = wf_steps.index(args.step)
    # create index range of methods
    if args.mode == "only":
        methods_range = [step]
    elif args.mode == "from":
        methods_range = range(step, len(wf_steps))
    elif args.mode in ("upto", "all"):
        methods_range = range(step+1)

    # execute workflow pipeline
    with directory_utils.change_to_working_directory(workingdir):
        for x in methods_range:
            method = pipeline[x][0]
            arguments = pipeline[x][1]
            title = pipeline[x][2]
            logger.info(method_header(title))
            # execute method
            method(**arguments)
            if not args.keep_backup:
                # remove backup files
                directory_utils.remove_backups(workingdir)


if __name__ == "__main__":
    main()
