import sys

import argparse

from BCEwf.utils.config import wf_steps, BCE_header


def create_parser():
    parser = argparse.ArgumentParser(usage=BCE_header)

    # create the parser for PDB+ligand code type of input
    parser.add_argument(
        "pdb_code",
        type=lambda s: s.upper(),
        help="input protein's PDB code")
    parser.add_argument(
        "ligand_code",
        type=lambda s: s.upper(),
        help="ligand code")
    parser.add_argument(
        "-protein_file",
        help="input protein's PDB file (optional)")
    ligand_input = parser.add_mutually_exclusive_group()
    ligand_input.add_argument(
        "-lig_file",
        help="input ligand's PDB file (optional)")
    ligand_input.add_argument(
        "-smiles_file",
        help="input ligand's SMILES file (optional)")
    ligand_input.add_argument(
        "-smiles_code",
        help="input ligand's SMILES code (optional)")

    # optional arguments
    parser.add_argument(
        "-workingdir",
        default="BCE_workflow_files",
        help="name of the project's working directory")
    parser.add_argument(
        "-acpype_path",
        help="path to the acpype.py file")
    parser.add_argument(
        "-aux_files_path",
        help="path to aux_files directory with all the auxiliary files")
    parser.add_argument(
        "-angle_unit",
        default="radians",
        choices="radians degrees".split(),
        help="angle unit to use in all dihedral angles matrices")
    parser.add_argument(
        "-max_noise",
        default=600,
        help="maximum noise tolerated in clustering post-processing")
    parser.add_argument(
        "-boxsize",
        default=0.8,
        type=float,
        help="Box size in REMD simulations in nm")
    parser.add_argument(
        "-boxtype",
        default="octahedron",
        help="Box type in REMD simulations",
        choices="triclinic cubic dodecahedron octahedron".split())
    parser.add_argument(
        "-cluster",
        default=0.05,
        type=float,
        help="clustering RMSD cut-off for two "
        "structures to be neighbors in nm")
    parser.add_argument(
        "-clMethod",
        default="gromos",
        help="clustering method",
        choices=[
            "gromos",
            "linkage",
            "jarvis-patrick",
            "monte-carlo",
            "diagonalization"])
    parser.add_argument(
        "-clModels",
        default=10,
        type=int,
        help="Maximum number of cluster representatives")
    parser.add_argument(
        "-clPercentage",
        default=95,
        type=int,
        help="percentage of trajectory snapshots represented by clusters")
    parser.add_argument(
        "-clIncrement",
        default=0.005,
        type=float,
        help="cutoff increment for iterative clustering in nm")
    parser.add_argument(
        "-reps",
        default=8,
        type=int,
        help="number of MD replicas in Gromacs REMD")
    parser.add_argument(
        "-postp_rep",
        default=0,
        type=int,
        help="replica number to be used in post-processing and clustering")
    parser.add_argument(
        "-eq_steps",
        default=1,
        type=int,
        help="number of equilibration steps")
    parser.add_argument(
        "-REMDeq_dt",
        default=0.001,
        type=float,
        help="time step for REMD equilibration")
    parser.add_argument(
        "-gmx_nsteps",
        default=1000000,
        type=int,
        help="number of steps to use in each "
        "REMD equilibration step MD run")
    parser.add_argument(
        "-procs",
        default=1,
        type=int,
        help="number of processors per MD replica")
    parser.add_argument(
        "-length",
        default=10,
        type=float,
        help="length of REMD simulation in nanoseconds")
    parser.add_argument(
        "-replex",
        default=500,
        type=int,
        help="number of steps between replica exchanges")
    parser.add_argument(
        "-maxwarn",
        default=2,
        type=int,
        help="number of maximum warnings allowed in Gromacs' grompp")
    parser.add_argument(
        "-tlow",
        default=298,
        type=int,
        help="initial lower temperature in REMD (K")
    parser.add_argument(
        "-thigh",
        default=398,
        type=int,
        help="final higher temperature in REMD (K)")
    parser.add_argument(
        "-ensemble",
        default="npt",
        help="REMD ensemble",
        choices="npt nvt".split())
    parser.add_argument(
        "-charge",
        type=int,
        help="charge to be passed to ACPYPE during parametrization",
        choices=[0, 1, -1])
    parser.add_argument(
        "-ph",
        default=7.4,
        type=float,
        help="pH used during ligand parametrization when adding Hydrogens")
    parser.add_argument(
        "-erasing_method",
        choices="gamma QM_energy".split(),
        default="gamma",
        help="energy erasing method to be usde in clustering post-processing")
    parser.add_argument(
        "-mode",
        choices="only upto from all".split(),
        default="all",
        help="choose mode of execution: "
        "only [step], up to [step], from [step] or all steps. "
        "Use with --executionStep flag.")
    parser.add_argument(
        "-gmx_exec",
        choices="gmx gmx_mpi".split(),
        default="gmx",
        help="Choose Gromacs executable.")
    parser.add_argument(
        "-step",
        choices=wf_steps,
        default="gauss",
        help="choose workflow step for partial execution. "
        "Use with --executionMode flag. "
        "ligext: ligand extraction. "
        "param: ligand parametrization. "
        "REMDeq: REMD equilibration. "
        "REMDprod: REMD production. "
        "cluster: clustering. "
        "dihedral: dihedral calculations. "
        "postpro: clustering post-processing. "
        "gauss: gaussian energy optimization.")

    # flags
    parser.add_argument(
        "--use_hrex",
        action='store_true',
        help="Set to use Hamiltonian Replica Exchange during production")

    parser.add_argument(
        "--use_slurm",
        action='store_true',
        help="Set to use Slurm for job scheduling")

    parser.add_argument(
        "--use_compss",
        action='store_true',
        help="Set to use PyCompss for job scheduling")

    parser.add_argument(
        "--force",
        action='store_true',
        help="force re-compute all steps")

    parser.add_argument(
        "--verbose",
        action='store_true',
        help="print debug messages")

    parser.add_argument(
        "--keep_backup",
        action='store_true',
        help="don't remove backup files created by Gromacs")

    parser.add_argument(
        "--use_given_workingdir",
        action='store_true',
        help="Use given working directory instead of creating "
        "another one when it already exists")
    return parser


def parse_args(args):
    parser = create_parser()
    if len(args) == 0:
        parser.print_help(sys.stderr)
        sys.exit(1)
    return parser.parse_args(args)
